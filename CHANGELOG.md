# Changelog - PixelPay Java Standalone SDK
Todos los cambios notables de este proyecto se documentarán en este archivo. Los registros de cambios son *para humanos*, no para máquinas, y la última versión es lo primero a mostrar.

El formato se basa en [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
y este proyecto se adhiere a [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
Tipos de cambios: `Added`, `Changed`, `Deprecated`, `Removed`, `Fixed`, `Security`.

## [v2.2.2] - 2024-02-26
### Fixed
- Se arreglan códigos de estados en Nicaragua, Reino Unido, Estados Unidos, Guatemala, Panamá, Perú y Portugal

## [v2.2.1] - 2023-10-10
### Added
- Se mejoró la firma heredada

## [v2.2.0] - 2023-09-21
### Removed
- Se eliminó lógica de encriptación

## [v2.1.9] - 2023-08-18
### Fixed
- Se ajustó tipo de campo `transaction_redeemed_points` a `double`

## [v2.1.8] - 2023-08-08
### Fixed
- Se excluyen campos encryptable y sdk_key_pair de serialización a JSON

## [v2.1.7] - 2023-08-08
### Added
- Se agregó método getSDKKeyPair para obtener llaves de encriptación

## [v2.1.6] - 2023-08-07
### Fixed
- Se arregló conflicto de encriptación al reutilizar instancias de `RequestBehaviour`
- Se arregló servicios de tokenización enviando fecha de expiración incorrecta si no se llenan los campos

## [v2.1.5] - 2023-07-28
### Fixed
- Se arregló acceso a propiedad protegida

## [v2.1.4] - 2023-07-27
### Added
- Se omiten los métodos de encriptación si la petición no lo requiere

### Changed
- En caso de fallos, se hacen reintentos para adquirir la llave pública del comercio
- En caso de fallos, se hacen reintentos para encriptar los datos de tarjeta

### Fixed
- Se corrigió caso donde reintentos de transacciones con una misma instancia de `RequestBehaviour` encripta datos múltiples veces

## [v2.1.3] - 2023-07-25
### Fixed
- Se corrigió método para obtener par de llaves creando nueva instancia de librería

### Changed
- Rollback de cambio versión de librería de JNA

## [v2.1.2] - 2023-07-25
### Changed
- Se cambió versión de librería de JNA

## [v2.1.1] - 2023-07-24
### Added
- Se agregó método para inicializar librería de Sodium con instancia existente.

## [v2.1.0] - 2023-07-18
### Added
- Se agregó encriptación de datos de tarjeta.

## [v2.0.9] - 2023-06-15
### Fixed
- Se corrige asignación de propiedad `sdk_version`.

## [v2.0.8] - 2023-06-14
### Fixed
- Se corrige llamado a archivo para consultar version del sdk.
- Se corrige test de cuotas, puntos y transacción de anulación.

## [v2.0.7] - 2023-05-30
### Added
- Se agregan campos de cuotas y puntos.
- Se agrega firma de transacciones de anulación.
- Se agrega función para obtener listado de formatos de teléfono y zip.

## [v2.0.6] - 2023-05-26
### Added
- Se agregan campos de programas.

### Fixed
- Se corrige comillas en propiedad sdk_version.

## [v2.0.5] - 2022-11-15
### Added
- Se muestra versión de SDK correcta en la respuesta.

## [v2.0.4] - 2022-10-20
### Added
- Se corrige error de response vacía.

## [v2.0.3] - 2022-10-20
### Updated
- Se corrige error de null pointer exception al recibir respuestas fallidas.

## [v2.0.2] - 2022-05-08
### Added
- Se agregan dependencias Jackson core y annotations

## [v2.0.1] - 2022-05-07
### Added
- Se agrega soporte para procesar respuestas desde un string JSON
- Se agrega el método fromJson en el modelo base Response

## [v2.0.0-beta] - 2022-04-22
### Added
- Se publica primer version de pruebas