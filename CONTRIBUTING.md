# Java Standalone SDK

## ¿Como publicar?

- Cambiar version de release en lib/build.gradle y en propiedad `sdk_version` en el archivo `RequestBehaviour.java`
- Agregar al CHANGELOG.md las descripciones del cambio
- Crear un commit con los cambios y agregar una etiqueta con el numero de version y hacer push con los tags

## Pruebas
Ejecutar todos los unit tests:
```bash
gradle test
```
Ejecutar unit test en particular:
```bash
gradle test --tests com.pixel.sdk.services.TransactionTests.testSuccessSaleTransactionWithEntity
```

## Util
Para ejecutar las pruebas de este SDK se debe instalar `Gradle`, se puede utilizar `Homebrew` para esto con el siguiente comando:
```
brew install gradle
```