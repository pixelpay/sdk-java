package com.pixel.sdk.helpers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.pixel.sdk.base.Helpers;
import com.pixel.sdk.models.Card;

import org.junit.jupiter.api.Test;

public class HelpersTest {

	@Test
	void testEmptyObjetToJson() {
		Object obj = new Object();
		String result = Helpers.objectToJson(obj);

		assertEquals("{}", result);
	}

	@Test
	void testObjectToJson() {
		Card card = new Card();
		card.cardholder = "Jhon Doe";
		card.cvv2 = "009";
		card.expire_month = 12;
		card.expire_year = 2025;
		card.number = "41111";

		String result = Helpers.objectToJson(card);

		assertEquals(11, result.lastIndexOf(card.number));
	}

	@Test
	void testEmptyHelperHash() {
		String result = Helpers.hash("nani", "json");
		assertEquals("", result);
	}

	@Test
	void testHelperHash() {
		assertEquals("466deec76ecdf5fca6d38571f6324d54", Helpers.hash("md5", "json"));
		assertEquals("05d97e6e9834ccf063c552e404b9ecafc5e4d662", Helpers.hash("SHA-1", "json"));
		assertEquals("02bd175f329720378ce83dd56a1b6b1f5291a60182d6c54b5e0d1e8d248a267a", Helpers.hash("SHA-256", "json"));
	}

	@Test
	void testTrimValue() {
		String result = Helpers.trimValue("  string ");
		String expected = "string";
		assertEquals(expected, result);
	}

	@Test
	void testParseAmount() {
		Double amount = 1.00;
		String expected = "1.0";

		assertEquals(expected, Helpers.parseAmount(amount));
	}
}
