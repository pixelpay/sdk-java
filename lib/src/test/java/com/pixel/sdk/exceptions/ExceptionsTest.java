package com.pixel.sdk.exceptions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.pixel.sdk.models.Settings;
import com.pixel.sdk.requests.StatusTransaction;
import com.pixel.sdk.services.Transaction;

import org.junit.jupiter.api.Test;

public class ExceptionsTest {

	@Test
	void testInvalidMerchantCredentials() {
		Exception exception = assertThrows(InvalidCredentialsException.class, () -> {

			Settings merchant = new Settings();
			merchant.setupEndpoint("https://0aef-187-254-146-176.ngrok.io");
			StatusTransaction status = new StatusTransaction();
			status.payment_uuid = "P-e21b135d-5605-4fc5-b31e-a9b77e1fe00";

			Transaction transaction = new Transaction(merchant);

			transaction.getStatus(status);
		});

		String expectedMessage = "The merchant credentials are not definied (key/hash).";
		String actualMessage = exception.getMessage();

		assertEquals(expectedMessage, actualMessage);
	}
}
