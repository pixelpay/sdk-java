package com.pixel.sdk.resources;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Map;

import org.junit.jupiter.api.Test;

public class LocationsTests {
	@Test
	void testCountriesList() {
		assertTrue(Locations.countriesList() instanceof Map);
		assertEquals("Honduras", Locations.countriesList().get("HN"));
	}

	@Test
	void testStatesList() {
		assertTrue(Locations.statesList("HN") instanceof Map);
		assertEquals("Cortes", Locations.statesList("HN").get("HN-CR"));
		assertEquals("Guatemala", Locations.statesList("GT").get("GT-01"));
		assertEquals("Costa Caribe Norte", Locations.statesList("NI").get("NI-AN"));
		assertEquals("Lancashire", Locations.statesList("GB").get("GB-LAN"));
	}

	@Test
	void testFormatsList() {
		assertTrue(Locations.formatsList("HN") instanceof Map);
		assertEquals("504", Locations.formatsList("HN").get("phone_code"));
		assertEquals("", Locations.formatsList("HN").get("zip_format"));
	}
}
