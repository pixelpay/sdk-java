package com.pixel.sdk.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

public class MerchantSettingsTest {

	@Test
	void testSetupMerchant() {
		Settings merchant = new Settings();
		String endpoint = "https://pixel-pay.com";
		assertEquals("https://pixelpay.app", merchant.endpoint);

		merchant.setupEndpoint(endpoint);
		assertEquals("https://pixel-pay.com", merchant.endpoint);

	}

	@Test
	void testSetupCredentials() {
		Settings merchant = new Settings();
		String merchant_key = "2212294583";
		String merchant_hash = "c7dd7a6cf3e47417edb3456c22218a5d";

		assertNull(merchant.auth_key);
		assertNull(merchant.auth_hash);

		merchant.setupCredentials(merchant_key, merchant_hash);

		assertNotNull(merchant.auth_key);
		assertNotNull(merchant.auth_hash);
	}

	@Test
	void testSetupPlatformUser() {
		Settings merchant = new Settings();
		String merchant_user = "c218697e71f1c8bb9efee480a9e3b90a930d8cfa662db3e3b78879ea38363dad7eefb9ebe8db582ba35cc46fa7d80baedf778195faecaea66d9222036793c937";

		assertNull(merchant.auth_user);
		merchant.setupPlatformUser(merchant_user);
		assertNotNull(merchant.auth_user);
	}

	@Test
	void testSetupEnvironment() {
		Settings merchant = new Settings();

		assertNull(merchant.environment);

		merchant.setupEnvironment("test");
		assertNotNull(merchant.environment);
		assertEquals("test", merchant.environment);
	}

	@Test
	void testSetupSandbox() {
		Settings merchant = new Settings();
		String sandbox_endpoint = "https://pixel-pay.com";
		String sandbox_auth = "1234567890";
		String sandbox_hash = "36cdf8271723276cb6f94904f8bde4b6";
		String sandbox_env = "sandbox";

		assertNotNull(merchant.endpoint, sandbox_endpoint);
		assertNull(merchant.auth_key);
		assertNull(merchant.auth_hash);
		assertNull(merchant.environment);

		merchant.setupSandbox();
		assertEquals(merchant.endpoint, sandbox_endpoint);
		assertEquals(merchant.auth_key, sandbox_auth);
		assertEquals(merchant.auth_hash, sandbox_hash);
		assertEquals(merchant.environment, sandbox_env);
	}

	@Test
	void testInherithSignature() {
		Settings merchant = new Settings();
		String sdk = "sdk-test";
		String sdk_version = "unreleased";

		merchant.sdk = sdk;
		merchant.sdk_version = sdk_version;

		assertEquals(sdk, merchant.sdk);
		assertEquals(sdk_version, merchant.sdk_version);
	}

}
