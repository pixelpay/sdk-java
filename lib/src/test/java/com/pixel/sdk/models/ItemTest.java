package com.pixel.sdk.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

public class ItemTest {

	@Test
	void testItemEmpty() {
		Item item = new Item();

		assertEquals(item.price, 0.00);
		assertEquals(item.qty, 1);
		assertEquals(item.tax, 0.00);
		assertEquals(item.total, 0.00);

		assertNull(item.code);
		assertNull(item.title);
	}

	@Test
	void testSetItem() {
		Item item = new Item();

		item.code = "ITEM-1";
		item.title = "Nintendo Swith";
		item.price = 6799;
		item.qty = 1;
		item.tax = 1072;
		item.totalize();

		assertEquals(6799, item.total);
		item.qty = 2;
		assertNotEquals(6799 * 2, item.total);
		item.totalize();
		assertEquals(6799 * 2, item.total);
	}

}
