package com.pixel.sdk.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

public class BillingTest {

	@Test
	void testBillingIsEmpty() {
		Billing billing = new Billing();

		assertNull(billing.address);
		assertNull(billing.country);
		assertNull(billing.state);
		assertNull(billing.city);
		assertNull(billing.zip);
		assertNull(billing.phone);
	}

	@Test
	void testSetBillingModel() {
		Billing billing = new Billing();

		billing.address = "Bo Andes";
		billing.country = "HN";
		billing.state = "Cortes";
		billing.city = "Puerto Vallarta";
		billing.zip = "48290";
		billing.phone = "3221002040";

		assertNotNull(billing.address);
		assertNotNull(billing.country);
		assertNotNull(billing.state);
		assertNotNull(billing.city);
		assertNotNull(billing.zip);
		assertNotNull(billing.phone);

		assertEquals("Bo Andes", billing.address);
		assertEquals("HN", billing.country);
		assertEquals("Cortes", billing.state);
		assertEquals("Puerto Vallarta", billing.city);
		assertEquals("48290", billing.zip);
		assertEquals("3221002040", billing.phone);
	}

}
