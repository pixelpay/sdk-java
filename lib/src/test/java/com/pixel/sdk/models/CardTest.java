package com.pixel.sdk.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

public class CardTest {

	@Test
	void testCardIsEmpty() {
		Card card = new Card();

		assertNull(card.number);
		assertNull(card.cvv2);
		assertEquals(0, card.expire_month);
		assertEquals(0, card.expire_year);
		assertNull(card.cardholder);
	}

	@Test
	void testSetupCard() {
		Card card = new Card();

		card.number = "4111 4111 4111 4111";
		card.cvv2 = "009";
		card.expire_year = 2025;
		card.expire_month = 12;
		card.cardholder = "Carlos Agaton";

		assertNotNull(card.number);
		assertNotNull(card.cvv2);
		assertNotNull(card.expire_month);
		assertNotNull(card.expire_year);
		assertNotNull(card.cardholder);

		assertEquals(card.number, "4111 4111 4111 4111");
		assertEquals(card.cvv2, "009");
		assertEquals(card.expire_month, 12);
		assertEquals(card.expire_year, 2025);
		assertEquals(card.cardholder, "Carlos Agaton");
	}

	@Test
	void testGetExpireFormat() {
		Card card = new Card();

		card.expire_month = 12;
		card.expire_year = 2025;

		assertEquals("2512", card.getExpireFormat());
	}
}
