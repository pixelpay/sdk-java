package com.pixel.sdk.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class OrderTest {

	@Test
	void testOrderIsEmpty() {
		Order order = new Order();

		assertNull(order.id);
		assertNull(order.currency);
		assertEquals(0.00, order.amount);
		assertEquals(0.00, order.tax_amount);
		assertEquals(0.00, order.shipping_amount);
		assertTrue(order.content.isEmpty());
		assertTrue(order.extras.isEmpty());
		assertNull(order.note);
		assertNull(order.callback_url);
		assertNull(order.customer_name);
		assertNull(order.customer_email);
	}

	@Test
	void testSetOrder() {
		Order order = new Order();

		order.id = "TEST-CASE";
		order.currency = "MXN";
		order.amount = 10.00;
		order.tax_amount = 5.00;
		order.shipping_amount = 5.00;
		order.note = "Nota";
		order.callback_url = "https://www.pixel.hn/es";
		order.customer_name = "Jhon Doe";
		order.customer_email = "jhond@pixel.hn";

		assertNotNull(order.id);
		assertNotNull(order.currency);
		assertNotNull(order.amount);
		assertNotNull(order.tax_amount);
		assertNotNull(order.shipping_amount);
		assertTrue(order.content.isEmpty());
		assertTrue(order.extras.isEmpty());
		assertNotNull(order.note);
		assertNotNull(order.callback_url);
		assertNotNull(order.customer_name);
		assertNotNull(order.customer_email);
		assertEquals(order.amount, 10.00);

		order.totalize();

		assertEquals(order.amount, 10.00);
	}

	@Test
	void testOrderWithItems() {
		Order order = new Order();
		Item item = new Item();

		item.code = "XCTA-1u";
		item.title = "XCTA-TEST";
		item.price = 5.00;
		item.qty = 5;
		item.tax = 10;

		order.addItem(item);
		order.totalize();

		assertEquals(item.price * item.qty, order.amount);
		assertEquals(item.tax * item.qty, order.tax_amount);
	}

	@Test
	void testOrderWithExtras() {
		Order order = new Order();
		order.addExtra("id", "123");

		assertFalse(order.extras.isEmpty());
		assertFalse(order.extras.containsKey("order"));
		assertTrue(order.extras.containsKey("id"));
	}
}
