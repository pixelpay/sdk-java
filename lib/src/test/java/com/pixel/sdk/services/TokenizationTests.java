package com.pixel.sdk.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.concurrent.CompletableFuture;

import com.pixel.sdk.base.Response;
import com.pixel.sdk.models.Billing;
import com.pixel.sdk.models.Card;
import com.pixel.sdk.models.Settings;
import com.pixel.sdk.requests.CardTokenization;
import com.pixel.sdk.responses.ErrorResponse;
import com.pixel.sdk.responses.InputErrorResponse;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

public class TokenizationTests {

	private Card getCardModel() {
		Card card = new Card();

		card.cardholder = "Jhon Doe";
		card.cvv2 = "009";
		card.expire_month = 12;
		card.expire_year = 2025;
		card.number = "4684668223050294";

		return card;
	}

	private Billing getBillingModel() {
		Billing billing = new Billing();
		billing.address = "Bo Andes";
		billing.city = "San Pedro Sula";
		billing.country = "HN";
		billing.phone = "999-9999999";
		billing.state = "HN-CR";
		billing.zip = "48338";

		return billing;
	}

	private Tokenization setupService() {
		Settings merchant = new Settings();
		merchant.setupSandbox();

		return new Tokenization(merchant);
	}

	@Test
	@Order(1)
	void testFailRequestForInvalidMerchant() throws IllegalStateException {
		Tokenization service = this.setupService();

		CompletableFuture<Response> future = CompletableFuture.supplyAsync(() -> {
			try {
				Response response = service.showCard("T-34e382e0-6f48-433b-b66d-cd2b5575939b");
				return response;

			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		});

		try {
			Response resp = future.get();
			assertFalse(resp.success);
			assertEquals(400, resp.getStatus());
			assertTrue(resp instanceof ErrorResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	@Order(2)
	void testVaultCardFail() {
		Tokenization service = this.setupService();
		CardTokenization cardTokenization = new CardTokenization();
		Card card = this.getCardModel();

		cardTokenization.setCard(card);

		CompletableFuture<Response> future = CompletableFuture.supplyAsync(() -> {
			try {
				Response response = service.vaultCard(cardTokenization);
				return response;

			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		});

		try {
			Response resp = future.get();
			assertFalse(resp.success);
			assertEquals(422, resp.getStatus());
			assertTrue(resp instanceof InputErrorResponse);
			
		} catch (Exception e) {
			assertTrue(false);
		}
	}

	@Test
	@Order(3)
	void testVaultCardSuccess() {
		Tokenization service = this.setupService();
		CardTokenization cardTokenization = new CardTokenization();
		Card card = this.getCardModel();
		Billing billing = this.getBillingModel();

		cardTokenization.setCard(card);
		cardTokenization.setBilling(billing);

		CompletableFuture<Response> future = CompletableFuture.supplyAsync(() -> {
			try {
				Response response = service.vaultCard(cardTokenization);
				return response;

			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		});

		try {
			Response resp = future.get();
			assertTrue(resp.success);
			assertEquals(200, resp.getStatus());
		} catch (Exception e) {
			assertTrue(false);
		}
	}

	@Test
	@Order(4)
	void testUpdateCardSuccess() {
		Tokenization service = this.setupService();
		CardTokenization cardTokenization = new CardTokenization();

		Card card = this.getCardModel();
		Billing billing = this.getBillingModel();

		cardTokenization.setCard(card);
		cardTokenization.setBilling(billing);

		Card update_card = new Card();
		update_card.number = "4111111111111111";

		Billing update_billing = new Billing();
		update_billing.city = "Choloma";

		CardTokenization cardUpdateTokenization = new CardTokenization();
		cardUpdateTokenization.setCard(update_card);
		cardUpdateTokenization.setBilling(update_billing);

		CompletableFuture<Response> future = CompletableFuture.supplyAsync(() -> {
			try {
				Response vault_response = service.vaultCard(cardTokenization);

				Response response = service.updateCard(vault_response.getData("token").toString(), cardUpdateTokenization);
				return response;

			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		});

		try {
			Response resp = future.get();
			assertTrue(resp.success);
			assertEquals(200, resp.getStatus());
		} catch (Exception e) {
			assertTrue(false);
		}
	}

	@Test
	@Order(5)
	void testDeleteCard() {
		Tokenization service = this.setupService();
		CardTokenization cardTokenization = new CardTokenization();

		Card card = this.getCardModel();
		Billing billing = this.getBillingModel();

		cardTokenization.setCard(card);
		cardTokenization.setBilling(billing);

		CompletableFuture<Response> future = CompletableFuture.supplyAsync(() -> {
			try {
				Response vault_response = service.vaultCard(cardTokenization);
				
				Response response = service.deleteCard(vault_response.getData("token").toString());
				return response;

			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		});

		try {
			Response resp = future.get();
			assertTrue(resp.success);
			assertEquals(200, resp.getStatus());
		} catch (Exception e) {
			assertTrue(false);
		}
	}

}