package com.pixel.sdk.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.pixel.sdk.base.Response;
import com.pixel.sdk.entities.TransactionResult;
import com.pixel.sdk.exceptions.InvalidCredentialsException;
import com.pixel.sdk.models.Billing;
import com.pixel.sdk.models.Card;
import com.pixel.sdk.models.Item;
import com.pixel.sdk.models.Order;
import com.pixel.sdk.models.Settings;
import com.pixel.sdk.requests.SaleTransaction;
import com.pixel.sdk.responses.ErrorResponse;
import com.pixel.sdk.responses.FailureResponse;
import com.pixel.sdk.responses.NotFoundResponse;
import com.pixel.sdk.responses.PaymentDeclinedResponse;
import com.pixel.sdk.responses.PreconditionalResponse;
import com.pixel.sdk.responses.TimeoutResponse;

import org.junit.jupiter.api.Test;

public class TransactionTests {

	private Billing getBillingModel() {
		Billing billing = new Billing();
		billing.address = "Ave Circunvalacion";
		billing.country = "HN";
		billing.state = "HN-CR";
		billing.city = "San Pedro Sula";
		billing.phone = "95852921";

		return billing;
	}

	private Card getCardModel() {
		Card card = new Card();
		card.number = "4111111111111111";
		card.cvv2 = "009";
		card.expire_month = 12;
		card.expire_year = 2023;
		card.cardholder = "IVAN SUAZO";

		return card;
	}

	private Order getOrderModel(Double amount) {
		Item item = new Item();
		item.code = "00001";
		item.title = "Example product";
		item.price = 1.99;
		item.qty = 2;

		Order order = new Order();
		order.id = UUID.randomUUID().toString();
		order.amount = amount;
		order.currency = "HNL";
		order.customer_email = "carlos@pixel.hn";
		order.customer_name = "Carlos Agaton";

		return order;
	}

	private void proccessPreconditionalTest(int case_number)
	{
		CompletableFuture<Response> future = CompletableFuture.supplyAsync(() -> {
			try {
				Settings merchant = new Settings();
				merchant.setupSandbox();

				Transaction transaction = new Transaction(merchant);
				SaleTransaction saleTx = new SaleTransaction();
				saleTx.setOrder(this.getOrderModel(case_number * 1.00));
				saleTx.setBilling(this.getBillingModel());
				saleTx.setCard(this.getCardModel());

				Response response = transaction.doSale(saleTx);

				return response;
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		});

		try {
			Response resp = future.get();
			assertFalse(resp.success);
			assertEquals(412, resp.getStatus());
			assertTrue(resp instanceof PreconditionalResponse);
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}

	@Test
	void testInvalidCredentials() throws InvalidCredentialsException {
		CompletableFuture<Response> future = CompletableFuture.supplyAsync(() -> {
			try {
				Settings merchant = new Settings();

				Transaction transaction = new Transaction(merchant);
				SaleTransaction saleTx = new SaleTransaction();

				Response response = transaction.doSale(saleTx);

				return response;
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		});

		try {
			future.get();
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue(e instanceof ExecutionException);
			assertTrue(e.getCause() instanceof IllegalStateException);
			assertTrue(e.getCause().getCause() instanceof InvalidCredentialsException);
		}
	}

	@Test
	void testErrorValidationSaleTransaction() throws IllegalStateException {
		CompletableFuture<Response> future = CompletableFuture.supplyAsync(() -> {
			try {
				Settings merchant = new Settings();
				merchant.setupSandbox();

				Transaction transaction = new Transaction(merchant);
				SaleTransaction saleTx = new SaleTransaction();

				Response response = transaction.doSale(saleTx);

				return response;
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		});

		try {
			Response resp = future.get();
			assertFalse(resp.success);
			assertEquals(422, resp.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}

	@Test
	void testFailSaleByInvalidCardFields() throws IllegalStateException {
		CompletableFuture<Response> future = CompletableFuture.supplyAsync(() -> {
			try {
				Settings merchant = new Settings();
				merchant.setupSandbox();

				Transaction transaction = new Transaction(merchant);
				SaleTransaction saleTx = new SaleTransaction();
				Card card = new Card();

				saleTx.setOrder(this.getOrderModel(1.00));
				saleTx.setBilling(this.getBillingModel());
				saleTx.setCard(card);

				Response response = transaction.doSale(saleTx);

				return response;
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		});

		try {
			Response resp = future.get();

			assertFalse(resp.success);
			assertEquals(422, resp.getStatus());
			assertTrue(resp.inputHasError("card_number"));
			assertTrue(resp.inputHasError("card_cvv"));
			assertTrue(resp.inputHasError("card_expire"));
			assertTrue(resp.inputHasError("card_holder"));
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}

	@Test
	void testFailSaleByInvalidBillingFields() throws IllegalStateException {
		CompletableFuture<Response> future = CompletableFuture.supplyAsync(() -> {
			try {
				Settings merchant = new Settings();
				merchant.setupSandbox();

				Transaction transaction = new Transaction(merchant);
				SaleTransaction saleTx = new SaleTransaction();
				Billing billing = new Billing();

				saleTx.setOrder(this.getOrderModel(1.00));
				saleTx.setCard(this.getCardModel());
				saleTx.setBilling(billing);

				Response response = transaction.doSale(saleTx);

				return response;
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		});

		try {
			Response resp = future.get();

			assertFalse(resp.success);
			assertEquals(422, resp.getStatus());
			assertTrue(resp.inputHasError("billing_address"));
			assertTrue(resp.inputHasError("billing_city"));
			assertTrue(resp.inputHasError("billing_state"));
			assertTrue(resp.inputHasError("billing_country"));
			assertTrue(resp.inputHasError("billing_phone"));
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}

	@Test
	void testFailSaleByInvalidOrderFields() throws IllegalStateException {
		CompletableFuture<Response> future = CompletableFuture.supplyAsync(() -> {
			try {
				Settings merchant = new Settings();
				merchant.setupSandbox();

				Transaction transaction = new Transaction(merchant);
				SaleTransaction saleTx = new SaleTransaction();
				Order order = new Order();

				saleTx.setOrder(order);
				saleTx.setCard(this.getCardModel());
				saleTx.setBilling(this.getBillingModel());

				Response response = transaction.doSale(saleTx);

				return response;
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		});

		try {
			Response resp = future.get();

			System.out.println(resp.toJson());
			
			assertFalse(resp.success);
			assertEquals(422, resp.getStatus());
			assertTrue(resp.inputHasError("customer_name"));
			assertTrue(resp.inputHasError("customer_email"));
			assertTrue(resp.inputHasError("order_id"));
			assertTrue(resp.inputHasError("order_currency"));
			assertTrue(resp.inputHasError("order_amount"));
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}

	@Test
	void testSuccessSaleTransaction() throws IllegalStateException {
		CompletableFuture<Response> future = CompletableFuture.supplyAsync(() -> {
			try {
				Settings merchant = new Settings();
				merchant.setupSandbox();

				Transaction transaction = new Transaction(merchant);
				SaleTransaction saleTx = new SaleTransaction();
				saleTx.setOrder(this.getOrderModel(1.00));
				saleTx.setBilling(this.getBillingModel());
				saleTx.setCard(this.getCardModel());

				Response response = transaction.doSale(saleTx);

				return response;
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		});

		try {
			Response resp = future.get();
			assertTrue(resp.success);
			assertEquals(200, resp.getStatus());
			assertEquals("sale", resp.getData("transaction_type"));
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}

	@Test
	void testSuccessSaleTransactionWithEntity() throws IllegalStateException {
		Settings merchant = new Settings();
		merchant.setupSandbox();

		Transaction transaction = new Transaction(merchant);
		SaleTransaction saleTx = new SaleTransaction();

		CompletableFuture<Response> future = CompletableFuture.supplyAsync(() -> {
			try {
				
				saleTx.setOrder(this.getOrderModel(1.00));
				saleTx.setBilling(this.getBillingModel());
				saleTx.setCard(this.getCardModel());

				Response response = transaction.doSale(saleTx);

				return response;
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		});

		try {
			Response resp = future.get();
			TransactionResult result = TransactionResult.fromResponse(resp);

			assertTrue(result.response_approved);
			assertTrue(transaction.verifyPaymentHash(result.payment_hash, saleTx.order_id, "@s4ndb0x-abcd-1234-n1l4-p1x3l"));
			assertEquals("sale", result.transaction_type);
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}

	@Test
	void testDeclinedSaleTransaction() throws IllegalStateException {
		CompletableFuture<Response> future = CompletableFuture.supplyAsync(() -> {
			try {
				Settings merchant = new Settings();
				merchant.setupSandbox();

				Transaction transaction = new Transaction(merchant);
				SaleTransaction saleTx = new SaleTransaction();
				saleTx.setOrder(this.getOrderModel(2.00));
				saleTx.setBilling(this.getBillingModel());
				saleTx.setCard(this.getCardModel());

				Response response = transaction.doSale(saleTx);

				return response;
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		});

		try {
			Response resp = future.get();
			assertFalse(resp.success);
			assertEquals(402, resp.getStatus());
			assertTrue(resp instanceof PaymentDeclinedResponse);
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}

	@Test
	void testInvalidSetupMerchantSettings() throws IllegalStateException {
		CompletableFuture<Response> future = CompletableFuture.supplyAsync(() -> {
			try {
				Settings merchant = new Settings();
				merchant.setupSandbox();

				Transaction transaction = new Transaction(merchant);
				SaleTransaction saleTx = new SaleTransaction();
				saleTx.setOrder(this.getOrderModel(3.00));
				saleTx.setBilling(this.getBillingModel());
				saleTx.setCard(this.getCardModel());

				Response response = transaction.doSale(saleTx);

				return response;
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		});

		try {
			Response resp = future.get();
			assertFalse(resp.success);
			assertEquals(400, resp.getStatus());
			assertTrue(resp instanceof ErrorResponse);
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}

	@Test
	void testFailedTransactionDueCardReport() throws IllegalStateException {
		CompletableFuture<Response> future = CompletableFuture.supplyAsync(() -> {
			try {
				Settings merchant = new Settings();
				merchant.setupSandbox();

				Transaction transaction = new Transaction(merchant);
				SaleTransaction saleTx = new SaleTransaction();
				saleTx.setOrder(this.getOrderModel(4.00));
				saleTx.setBilling(this.getBillingModel());
				saleTx.setCard(this.getCardModel());

				Response response = transaction.doSale(saleTx);

				return response;
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		});

		try {
			Response resp = future.get();
			assertFalse(resp.success);
			assertEquals(402, resp.getStatus());
			assertTrue(resp instanceof PaymentDeclinedResponse);
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}

	@Test
	void testPaymentNotFount() throws IllegalStateException {
		CompletableFuture<Response> future = CompletableFuture.supplyAsync(() -> {
			try {
				Settings merchant = new Settings();
				merchant.setupSandbox();

				Transaction transaction = new Transaction(merchant);
				SaleTransaction saleTx = new SaleTransaction();
				saleTx.setOrder(this.getOrderModel(5.00));
				saleTx.setBilling(this.getBillingModel());
				saleTx.setCard(this.getCardModel());

				Response response = transaction.doSale(saleTx);

				return response;
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		});

		try {
			Response resp = future.get();
			assertFalse(resp.success);
			assertEquals(406, resp.getStatus());
			assertTrue(resp instanceof NotFoundResponse);
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}

	@Test
	void testFailByLimitsExceeded() throws IllegalStateException {
		this.proccessPreconditionalTest(6);
	}

	@Test
	void testGeneralSystemFailure() throws IllegalStateException {
		CompletableFuture<Response> future = CompletableFuture.supplyAsync(() -> {
			try {
				Settings merchant = new Settings();
				merchant.setupSandbox();

				Transaction transaction = new Transaction(merchant);
				SaleTransaction saleTx = new SaleTransaction();
				saleTx.setOrder(this.getOrderModel(7.00));
				saleTx.setBilling(this.getBillingModel());
				saleTx.setCard(this.getCardModel());

				Response response = transaction.doSale(saleTx);

				return response;
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		});

		try {
			Response resp = future.get();
			assertFalse(resp.success);
			assertEquals(500, resp.getStatus());
			assertTrue(resp instanceof FailureResponse);
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}

	@Test
	void testTimeoutError() throws IllegalStateException {
		CompletableFuture<Response> future = CompletableFuture.supplyAsync(() -> {
			try {
				Settings merchant = new Settings();
				merchant.setupSandbox();

				Transaction transaction = new Transaction(merchant);
				SaleTransaction saleTx = new SaleTransaction();
				saleTx.setOrder(this.getOrderModel(8.00));
				saleTx.setBilling(this.getBillingModel());
				saleTx.setCard(this.getCardModel());

				Response response = transaction.doSale(saleTx);

				return response;
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		});

		try {
			Response resp = future.get();
			assertFalse(resp.success);
			assertEquals(408, resp.getStatus());
			assertTrue(resp instanceof TimeoutResponse);
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}

	@Test
	void testTransactionAmountExceeded() throws IllegalStateException {
		this.proccessPreconditionalTest(9);
	}

	@Test
	void testTransactionLimitExceeded() throws IllegalStateException {
		this.proccessPreconditionalTest(10);
	}

	@Test
	void testConfigurationLimits() throws IllegalStateException {
		this.proccessPreconditionalTest(11);
	}
}
