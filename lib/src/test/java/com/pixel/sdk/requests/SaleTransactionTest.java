package com.pixel.sdk.requests;

import com.pixel.sdk.base.Helpers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class SaleTransactionTest {
	@Test
	void testPaymentTransactionInstallment() {
		SaleTransaction sale = new SaleTransaction();

		sale.setInstallment(12, "extra");
		assertEquals("extra", sale.installment_type);
		assertEquals("12", sale.installment_months);
	}

	@Test
	void testPaymentTransactionPointsRedeem() {
		SaleTransaction sale = new SaleTransaction();

		sale.withPointsRedeemAmount(100);
		assertEquals("100.0", sale.points_redeem_amount);
	}
}