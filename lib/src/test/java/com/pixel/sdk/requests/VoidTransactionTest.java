package com.pixel.sdk.requests;

import com.pixel.sdk.base.Helpers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class VoidTransactionTest {
	@Test
	void testVoidSignature() {
		VoidTransaction void_tx = new VoidTransaction();

		void_tx.void_signature = "d206a1e51ffd42f0a8c4146d4a06846f560df211e28ec011e92b8dd4648d454f64be3ca50f753fb624f8c4c3585b3ebb6b3a954869818728f23a78b474a5b1fc";
		assertEquals(Helpers.hash("SHA-512", "sonia@pixel.hn|20230530|@s4ndb0x-abcd-1234-n1l4-p1x3l"), void_tx.void_signature);
	}
}