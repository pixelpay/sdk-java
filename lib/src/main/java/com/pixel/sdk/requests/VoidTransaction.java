package com.pixel.sdk.requests;

import com.pixel.sdk.base.RequestBehaviour;

public class VoidTransaction extends RequestBehaviour {
	/**
	 * Payment UUID
	 */
	public String payment_uuid;

	/**
	 * Reason for void the order
	 */
	public String void_reason;

	/**
	 * Required signature for void authentication
	 */
	public String void_signature;
}
