package com.pixel.sdk.requests;

import com.pixel.sdk.base.RequestBehaviour;

public class CaptureTransaction extends RequestBehaviour {
	/**
	 * Payment UUID
	 */
	public String payment_uuid;

	/**
	 * The total amount to capture, equal to or less than the authorized amount.
	 */
	public String transaction_approved_amount;
}
