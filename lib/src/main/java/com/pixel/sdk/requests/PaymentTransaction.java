package com.pixel.sdk.requests;

import java.util.Map;
import com.pixel.sdk.base.RequestBehaviour;
import com.pixel.sdk.base.Helpers;
import com.pixel.sdk.models.Billing;
import com.pixel.sdk.models.Card;
import com.pixel.sdk.models.Order;

public class PaymentTransaction extends RequestBehaviour {
	/**
	 * Payment UUID
	 */
	public String payment_uuid;

	/**
	 * Tokenized card identifier (T-* format)
	 */
	public String card_token;

	/**
	 * Card number or PAN
	 */
	public String card_number;

	/**
	 * Card security code
	 */
	public String card_cvv;

	/**
	 * Card expire year/month date (YYMM)
	 */
	public String card_expire;

	/**
	 * Cardholder name
	 */
	public String card_holder;

	/**
	 * Customer billing address
	 */
	public String billing_address;

	/**
	 * Customer billing country alpha-2 code (ISO 3166-1)
	 */
	public String billing_country;

	/**
	 * Customer billing state alpha code (ISO 3166-2)
	 */
	public String billing_state;

	/**
	 * Customer billing city
	 */
	public String billing_city;

	/**
	 * Customer billing postal code
	 */
	public String billing_zip;

	/**
	 * Customer billing phone
	 */
	public String billing_phone;

	/**
	 * Order customer name
	 */
	public String customer_name;

	/**
	 * Order customer email
	 */
	public String customer_email;

	/**
	 * Order customer device fingerprint
	 */
	public String customer_fingerprint;

	/**
	 * Order ID
	 */
	public String order_id;

	/**
	 * Order currency code alpha-3
	 */
	public String order_currency;

	/**
	 * Order total amount
	 */
	public String order_amount;

	/**
	 * Order total tax amount
	 */
	public String order_tax_amount;

	/**
	 * Order total shipping amount
	 */
	public String order_shipping_amount;

	/**
	 * Order summary of items or products
	 */
	public Object[] order_content;

	/**
	 * Order extra properties
	 */
	public Map<String, String> order_extras;

	/**
	 * Order note or aditional instructions
	 */
	public String order_note;

	/**
	 * Order calback webhook URL
	 */
	public String order_callback;

	/**
	 * Activate authentication request (3DS/EMV)
	 */
	public Boolean authentication_request = false;

	/**
	 * Authentication transaction identifier
	 */
	public String authentication_identifier;

	/**
	 * Associate and mapping Card model properties to transaction
	 * 
	 * @param card
	 */
	public void setCard(Card card) {
		this.card_number = Helpers.trimValue(card.number);
		this.card_cvv = card.cvv2;
		this.card_expire = card.getExpireFormat();
		this.card_holder = Helpers.trimValue(card.cardholder);
	}

	/**
	 * Associate and mapping CardToken model properties to transaction
	 * 
	 * @param token
	 */
	public void setCardToken(String token) {
		this.card_token = token;
	}

	/**
	 * Associate and mapping Billing model properties to transaction
	 * 
	 * @param billing
	 */
	public void setBilling(Billing billing) {
		this.billing_address = Helpers.trimValue(billing.address);
		this.billing_country = billing.country;
		this.billing_state = billing.state;
		this.billing_city = Helpers.trimValue(billing.city);
		this.billing_zip = billing.zip;
		this.billing_phone = billing.phone;
	}

	/**
	 * Associate and mapping Order model properties to transaction
	 * 
	 * @param order
	 */
	public void setOrder(Order order) {
		this.order_id = order.id;
		this.order_currency = order.currency;
		this.order_amount = Helpers.parseAmount(order.amount);
		this.order_tax_amount = Helpers.parseAmount(order.tax_amount);
		this.order_shipping_amount = Helpers.parseAmount(order.shipping_amount);
		this.order_content = order.content.isEmpty() ? null : order.content.toArray();
		this.order_extras = order.extras.isEmpty() ? null : order.extras;
		this.order_note = Helpers.trimValue(order.note);
		this.order_callback = order.callback_url;

		this.customer_name = Helpers.trimValue(order.customer_name);
		this.customer_email = order.customer_email;
	}

	/**
	 * Enable 3DS/EMV authentication request
	 */
	public void withAuthenticationRequest() {
		this.authentication_request = true;
	}
}
