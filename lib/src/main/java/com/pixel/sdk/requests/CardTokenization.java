package com.pixel.sdk.requests;

import com.pixel.sdk.base.Helpers;
import com.pixel.sdk.base.RequestBehaviour;
import com.pixel.sdk.models.Billing;
import com.pixel.sdk.models.Card;

public class CardTokenization extends RequestBehaviour {
	/**
	 * Card number or PAN
	 */
	public String number;

	/**
	 * Card security code
	 */
	public String cvv2;

	/**
	 * Card expire month date (MM)
	 */
	public String expire_month;

	/**
	 * Card expire year date (YYYY)
	 */
	public String expire_year;

	/**
	 * Cardholder name
	 */
	public String cardholder;

	/**
	 * Customer billing address
	 */
	public String address;

	/**
	 * Customer billing country alpha-2 code (ISO 3166-1)
	 */
	public String country;

	/**
	 * Customer billing state alpha code (ISO 3166-2)
	 */
	public String state;

	/**
	 * Customer billing city
	 */
	public String city;

	/**
	 * Customer billing postal code
	 */
	public String zip;

	/**
	 * Customer billing phone
	 */
	public String phone;

	/**
	 * Customer email
	 */
	public String email;

	/**
	 * Associate and mapping Card model properties to transaction
	 * 
	 * @param card
	 */
	public void setCard(Card card) {
		this.number = Helpers.trimValue(card.number);
		this.cvv2 = card.cvv2;
		this.cardholder = Helpers.trimValue(card.cardholder);
		
		if (card.expire_month != 0) {
			this.expire_month = String.format("%02d", card.expire_month);
		}

		if (card.expire_year != 0) {
			this.expire_year = String.valueOf(card.expire_year);
		}		
	}

	/**
	 * Associate and mapping Billing model properties to transaction
	 * 
	 * @param billing
	 */
	public void setBilling(Billing billing) {
		this.address = Helpers.trimValue(billing.address);
		this.country = billing.country;
		this.state = billing.state;
		this.city = Helpers.trimValue(billing.city);
		this.zip = billing.zip;
		this.phone = billing.phone;
	}
}
