package com.pixel.sdk.requests;

import com.pixel.sdk.base.Helpers;

public class SaleTransaction extends PaymentTransaction {
	/**
	 * Transaction installment type
	 */
	public String installment_type;

	/**
	 * Transaction installment value
	 */
	public String installment_months;

	/**
	 * Transaction total points redeem amount
	 */
	public String points_redeem_amount;

	/**
	 * Set Installment service values to transaction
	 *
	 * @param months
	 * @param type
	 */
	public void setInstallment(int months, String type) {
		this.installment_months = String.valueOf(months);
		this.installment_type = type;
	}

	/**
	 * Set transaction points redeem amount
	 *
	 * @param amount
	 */
	public void withPointsRedeemAmount(double amount) {
		this.points_redeem_amount = Helpers.parseAmount(amount);
	}
}
