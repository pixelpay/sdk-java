package com.pixel.sdk.services;

import com.pixel.sdk.exceptions.InvalidCredentialsException;

import com.pixel.sdk.base.RequestBehaviour;
import com.pixel.sdk.base.Response;
import com.pixel.sdk.base.ServiceBehaviour;
import com.pixel.sdk.models.Settings;
import com.pixel.sdk.requests.CardTokenization;

public class Tokenization extends ServiceBehaviour {
	private static final String BASE_CARD_PATH = "api/v2/tokenization/card";

	/**
	 * Initialize service
	 * 
	 * @throws InvalidCredentialsException
	 */
	public Tokenization(Settings settings) {
		super(settings);
	}

	/**
	 * Vault credit/debit card and obtain a token card identifier (T-* format)
	 * 
	 * @param card
	 * @return
	 * @throws InvalidCredentialsException
	 */
	public Response vaultCard(CardTokenization card) throws InvalidCredentialsException {
		return this.post(Tokenization.BASE_CARD_PATH, card);
	}

	/**
	 * Update credit/debit card by token card identifier
	 * 
	 * @param token
	 * @param card
	 * @return
	 * @throws InvalidCredentialsException
	 */
	public Response updateCard(String token, CardTokenization card) throws InvalidCredentialsException {
		return this.put(Tokenization.BASE_CARD_PATH + "/" + token, card);
	}

	/**
	 * Show credit/debit card metadata by token card identifier
	 * 
	 * @param token
	 * @return
	 * @throws InvalidCredentialsException
	 */
	public Response showCard(String token) throws InvalidCredentialsException {
		RequestBehaviour request = new RequestBehaviour();

		return this.get(Tokenization.BASE_CARD_PATH + "/" + token, request);
	}

	/**
	 * Show credit/debit cards metadata by tokens card identifier
	 * 
	 * @param tokens
	 * @return
	 * @throws InvalidCredentialsException
	 */
	public Response showCards(String[] tokens) throws InvalidCredentialsException {
		RequestBehaviour request = new RequestBehaviour();

		return this.get(Tokenization.BASE_CARD_PATH + "/" + String.join(":", tokens), request);
	}

	/**
	 * Delete credit/debit card metadata by token card identifier
	 * 
	 * @param token
	 * @return
	 * @throws InvalidCredentialsException
	 */
	public Response deleteCard(String token) throws InvalidCredentialsException {
		RequestBehaviour request = new RequestBehaviour();

		return this.delete(Tokenization.BASE_CARD_PATH + "/" + token, request);
	}
}
