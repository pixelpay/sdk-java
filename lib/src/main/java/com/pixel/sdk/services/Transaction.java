package com.pixel.sdk.services;

import com.pixel.sdk.exceptions.InvalidCredentialsException;
import com.pixel.sdk.exceptions.InvalidTransactionTypeException;
import com.pixel.sdk.base.ServiceBehaviour;

import java.io.IOException;

import com.pixel.sdk.base.Helpers;
import com.pixel.sdk.base.Response;
import com.pixel.sdk.models.Settings;
import com.pixel.sdk.requests.AuthTransaction;
import com.pixel.sdk.requests.CaptureTransaction;
import com.pixel.sdk.requests.PaymentTransaction;
import com.pixel.sdk.requests.SaleTransaction;
import com.pixel.sdk.requests.StatusTransaction;
import com.pixel.sdk.requests.VoidTransaction;

public class Transaction extends ServiceBehaviour {
	/**
	 * Initialize service
	 * 
	 * @throws InvalidCredentialsException
	 */
	public Transaction(Settings settings) {
		super(settings);
	}

	/**
	 * Evaluate if transactions should be 3DS authentication
	 * 
	 * @param transaction
	 * @throws InvalidTransactionTypeException
	 */
	private void evalAuthenticationTransaction(PaymentTransaction transaction) throws InvalidTransactionTypeException {
		if (!Boolean.TRUE.equals(transaction.authentication_request)) {
			return;
		}

		try {
			Class.forName("android.os.Build");
		} catch(ClassNotFoundException e) {
			throw new InvalidTransactionTypeException("This platform not support 3DS transactions");
		}
	}

	/**
	 * Send and proccesing SALE transaction
	 * 
	 * @param transaction
	 * @return
	 * @throws InvalidCredentialsException
	 * @throws IOException
	 */
	public Response doSale(SaleTransaction transaction) throws InvalidCredentialsException, InvalidTransactionTypeException {
		this.evalAuthenticationTransaction(transaction);

		return this.post("api/v2/transaction/sale", transaction);
	}

	/**
	 * Send and proccesing AUTH transaction
	 * 
	 * @param transaction
	 * @return
	 * @throws InvalidCredentialsException
	 * @throws IOException
	 */
	public Response doAuth(AuthTransaction transaction) throws InvalidCredentialsException, InvalidTransactionTypeException {
		this.evalAuthenticationTransaction(transaction);

		return this.post("api/v2/transaction/auth", transaction);
	}

	/**
	 * Send and proccesing CAPTURE transaction
	 * 
	 * @param transaction
	 * @return
	 * @throws InvalidCredentialsException
	 * @throws IOException
	 */
	public Response doCapture(CaptureTransaction transaction) throws InvalidCredentialsException {
		return this.post("api/v2/transaction/capture", transaction);
	}

	/**
	 * Send and proccesing VOID transaction
	 * 
	 * @param transaction
	 * @return
	 * @throws InvalidCredentialsException
	 * @throws IOException
	 */
	public Response doVoid(VoidTransaction transaction) throws InvalidCredentialsException {
		return this.post("api/v2/transaction/void", transaction);
	}

	/**
	 * Verify transaction status
	 * 
	 * @param transaction
	 * @return
	 * @throws InvalidCredentialsException
	 * @throws IOException
	 */
	public Response getStatus(StatusTransaction transaction) throws InvalidCredentialsException {
		return this.post("api/v2/transaction/status", transaction);
	}

	/**
	 * Verify a payment hash and returns true if payment response is not modified
	 * 
	 * @param hash
	 * @param order_id
	 * @param secret
	 * @return
	 */
	public boolean verifyPaymentHash(String hash, String order_id, String secret) {
		String chain = String.join("|", order_id, this.settings.auth_key, secret);

		return hash.equals(Helpers.hash("MD5", chain));
	}
}
