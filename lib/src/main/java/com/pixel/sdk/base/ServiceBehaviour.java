package com.pixel.sdk.base;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpResponseException;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.pixel.sdk.exceptions.InvalidCredentialsException;
import com.pixel.sdk.models.Settings;
import com.pixel.sdk.responses.FailureResponse;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

public class ServiceBehaviour {
	/**
	 * Settings service model
	 */
	protected Settings settings;

	/**
	 * Initialize service
	 */
	public ServiceBehaviour(Settings settings) {
		this.settings = settings;
	}

	/**
	 * Build HTTP request to API
	 *
	 * @param request
	 * @return
	 * @throws InvalidCredentialsException
	 */
	private Response buildRequest(HttpRequest request) throws IOException, InvalidCredentialsException {
		try {
			request.setFollowRedirects(true);

			if (this.settings.auth_key == null || this.settings.auth_hash == null) {
				throw new InvalidCredentialsException("The merchant credentials are not definied (key/hash).");
			}

			request.getHeaders()
				.set("x-auth-key", this.settings.auth_key)
				.set("x-auth-hash", this.settings.auth_hash)
				.setAccept("application/json");

			if (this.settings.auth_user != null) {
				request.getHeaders().set("x-auth-user", this.settings.auth_user);
			}

			HttpResponse response = request.execute();

			return Response.fromJson(response.parseAsString(), response.getStatusCode());
		} catch (HttpResponseException e) {
			if (e.getContent() == null) {
				return Response.fromJson(String.format("{\"message\":\"%s\"}", e.getMessage()), e.getStatusCode());
			}

			return Response.fromJson(e.getContent(), e.getStatusCode());
		}
	}

	/**
	 * Proccess the exception to Response object
	 *
	 * @param e
	 * @return
	 */
	private Response exceptionResponse(Throwable e) {
		Response response = new FailureResponse();
		response.status = 520;
		response.message = e.getMessage();

		return response;
	}

	/**
	 * Get API route
	 *
	 * @param route
	 * @return
	 */
	private String getRoute(String route) {
		return this.settings.endpoint + "/" + route;
	}

	/**
	 * Verify settings variables and inherith values
	 *
	 * @param request
	 */
	private void validateRequest(RequestBehaviour request) {
		if (this.settings.environment != null) {
			request.env = this.settings.environment;
		}

		if (this.settings.lang != null) {
			request.lang = this.settings.lang;
		}

		if (this.settings.sdk != null) {
			request.from = this.settings.sdk;
		}

		if (this.settings.sdk_version != null) {
			request.sdk_version = this.settings.sdk_version;
		}
	}

	/**
	 * API POST request
	 *
	 * @param url
	 * @param body
	 * @return
	 * @throws InvalidCredentialsException
	 */
	protected Response post(String url, RequestBehaviour body) throws InvalidCredentialsException {
		try {
			this.validateRequest(body);

			HttpRequestFactory factory = new NetHttpTransport().createRequestFactory();
			ByteArrayContent content = new ByteArrayContent("application/json", body.toJson().getBytes(StandardCharsets.UTF_8));
			HttpRequest request = factory.buildPostRequest(new GenericUrl(this.getRoute(url)), content);

			return this.buildRequest(request);
		} catch (IOException e) {
			return this.exceptionResponse(e);
		}
	}

	/**
	 * API PUT request
	 *
	 * @param url
	 * @param body
	 * @return
	 * @throws InvalidCredentialsException
	 */
	protected Response put(String url, RequestBehaviour body) throws InvalidCredentialsException {
		try {
			this.validateRequest(body);

			HttpRequestFactory factory = new NetHttpTransport().createRequestFactory();
			ByteArrayContent content = new ByteArrayContent("application/json", body.toJson().getBytes(StandardCharsets.UTF_8));
			HttpRequest request = factory.buildPutRequest(new GenericUrl(this.getRoute(url)), content);

			return this.buildRequest(request);
		} catch (IOException e) {
			return this.exceptionResponse(e);
		}
	}

	/**
	 * API DELETE request
	 *
	 * @param url
	 * @param body
	 * @return
	 * @throws InvalidCredentialsException
	 */
	protected Response delete(String url, RequestBehaviour body) throws InvalidCredentialsException {
		try {
			this.validateRequest(body);

			GenericUrl url_factory = new GenericUrl(this.getRoute(url));

			ObjectMapper mapper = new ObjectMapper();
			url_factory.putAll(mapper.convertValue(body, new TypeReference<HashMap<String, Object>>() {}));

			HttpRequestFactory factory = new NetHttpTransport().createRequestFactory();
			HttpRequest request = factory.buildDeleteRequest(url_factory);

			return this.buildRequest(request);
		} catch (IOException e) {
			return this.exceptionResponse(e);
		}
	}

	/**
	 * API GET request
	 *
	 * @param url
	 * @param body
	 * @return
	 * @throws InvalidCredentialsException
	 */
	protected Response get(String url, RequestBehaviour body) throws InvalidCredentialsException {
		try {
			this.validateRequest(body);

			GenericUrl url_factory = new GenericUrl(this.getRoute(url));

			ObjectMapper mapper = new ObjectMapper();
			url_factory.putAll(mapper.convertValue(body, new TypeReference<HashMap<String, Object>>() {}));

			HttpRequestFactory factory = new NetHttpTransport().createRequestFactory();
			HttpRequest request = factory.buildGetRequest(url_factory);

			return this.buildRequest(request);
		} catch (IOException e) {
			return this.exceptionResponse(e);
		}
	}
}
