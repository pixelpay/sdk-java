package com.pixel.sdk.base;

import java.util.Map;
import java.io.IOException;

import com.pixel.sdk.responses.ErrorResponse;
import com.pixel.sdk.responses.FailureResponse;
import com.pixel.sdk.responses.InputErrorResponse;
import com.pixel.sdk.responses.NetworkFailureResponse;
import com.pixel.sdk.responses.NoAccessResponse;
import com.pixel.sdk.responses.NotFoundResponse;
import com.pixel.sdk.responses.PayloadResponse;
import com.pixel.sdk.responses.PaymentDeclinedResponse;
import com.pixel.sdk.responses.PreconditionalResponse;
import com.pixel.sdk.responses.SuccessResponse;
import com.pixel.sdk.responses.TimeoutResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;

@JsonInclude(Include.NON_NULL)
public class Response {
	/**
	 * HTTP response status code
	 */
	protected int status;

	/**
	 * Exception response details
	 */
	protected Map<String, Object> exception;

	/**
	 * Response status success
	 */
	public boolean success;

	/**
	 * Response friendly message
	 */
	public String message;

	/**
	 * Response 'action to' format
	 */
	public String action;

	/**
	 * Response data payload
	 */
	public Map<String, Object> data;

	/**
	 * Response input validation felds errors
	 */
	public Map<String, String[]> errors;

	/**
	 * Define HTTP status code response
	 * 
	 * @param status
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * Get HTTP status code
	 * 
	 * @return
	 */
	public int getStatus() {
		return this.status;
	}

	/**
	 * Verify input has error
	 * 
	 * @param key
	 * @return
	 */
	public boolean inputHasError(String key) {
		if (this.errors == null) {
			return false;
		}
		
		return this.errors.containsKey(key);
	}

	/**
	 * Get data payload by key
	 * 
	 * @param key
	 * @return
	 */
	public Object getData(String key) {
		if (this.data == null) {
			return null;
		}

		return this.data.get(key);
	}

	/**
	 * Serialize object to JSON string
	 * 
	 * @return
	 */
	public String toJson() {
		return Helpers.objectToJson(this);
	}

	/**
	 * Mapping and cast HTTP response
	 *
	 * @param body
	 * @param status
	 * @return
	 */
	public static Response fromJson(String body, int status) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);

		try {
			Response bag = null;

			switch (status) {
			case 200:
				bag = mapper.readValue(body, SuccessResponse.class);
				break;

			case 202:
				bag = mapper.readValue(body, PayloadResponse.class);
				break;

			case 400:
				bag = mapper.readValue(body, ErrorResponse.class);
				break;

			case 401:
			case 403:
				bag = mapper.readValue(body, NoAccessResponse.class);
				break;

			case 402:
				bag = mapper.readValue(body, PaymentDeclinedResponse.class);
				break;

			case 404:
			case 405:
			case 406:
				bag = mapper.readValue(body, NotFoundResponse.class);
				break;

			case 408:
				bag = mapper.readValue(body, TimeoutResponse.class);
				break;

			case 412:
			case 418:
				bag = mapper.readValue(body, PreconditionalResponse.class);
				break;

			case 422:
				bag = mapper.readValue(body, InputErrorResponse.class);
				break;

			case 500:
				bag = mapper.readValue(body, FailureResponse.class);
				break;

			default:
				bag = status > 500 ? mapper.readValue(body, NetworkFailureResponse.class) : mapper.readValue(body, Response.class);
				break;
			}

			if (bag.status == 0) {
				bag.status = status;
			}

			return bag;
		} catch (IOException e) {
			return new Response();
		}
	}
}
