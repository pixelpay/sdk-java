package com.pixel.sdk.base;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Helpers {
	/**
	 * Prevent implicit public contructor
	 */
	private Helpers() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * Serialize object to JSON string without empties
	 *
	 * @param value
	 * @return
	 */
	public static String objectToJson(Object value) {
		ObjectMapper mapper = new ObjectMapper();

		try {
			mapper.setSerializationInclusion(Include.NON_NULL);
			return mapper.writeValueAsString(value);
		} catch (JsonProcessingException e) {
			e.printStackTrace();

			return "{}";
		}
	}

	/**
	 * Serialize JSON to object without empties
	 *
	 * @param <T>
	 * @param json
	 * @param type
	 * @return
	 */
	public static <T> T jsonToObject(String json, Class<T> type) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);

		try {
			return mapper.readValue(json, type);
		} catch (IOException e) {
			return null;
		}
	}

	/**
	 * Helper to hash object by algorithm
	 *
	 * @param algorithm
	 * @param value
	 * @return
	 */
	public static String hash(String algorithm, String value) {
		try {
			MessageDigest message = MessageDigest.getInstance(algorithm);
			byte[] digest = message.digest(value.getBytes(StandardCharsets.UTF_8));

			StringBuilder hash = new StringBuilder();

			for (byte b : digest) {
				hash.append(String.format("%02x", b));
			}

			return hash.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();

			return "";
		}
	}

	/**
	 * Trim a string/null value
	 *
	 * @param value
	 * @return
	 */
	public static String trimValue(String value) {
		if (value != null) {
			return value.trim();
		}

		return null;
	}

	/**
	 * Parse or nullify amount data
	 *
	 * @param amount
	 * @return
	 */
	public static String parseAmount(double amount) {
		if (amount > 0) {
			return String.valueOf(amount);
		}

		return null;
	}
}
