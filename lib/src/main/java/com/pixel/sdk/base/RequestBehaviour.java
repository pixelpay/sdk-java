package com.pixel.sdk.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;

public class RequestBehaviour {
	/**
	 * Environment identifier (live|test|sandbox)
	 */
	public String env;

	/**
	 * Transaction response messages language
	 */
	public String lang;

	/**
	 * SDK identifier type
	 */
	public String from;

	/**
	 * SDK version
	 */
	public String sdk_version;

	/**
	 * Initialize request
	 */
	public RequestBehaviour() {
		this.lang = Locale.getDefault().getLanguage();
		this.from = "sdk-java";

		if (!this.lang.equals("es") && !this.lang.equals("en")) {
			this.lang = "es";
		}

		this.sdk_version = "2.2.2";
	}

	/**
	 * Serialize object to JSON string
	 *
	 * @return
	 */
	public String toJson() {
		return Helpers.objectToJson(this);
	}
}
