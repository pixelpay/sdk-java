package com.pixel.sdk.exceptions;

public class InvalidTransactionTypeException extends Exception {
	public InvalidTransactionTypeException(String message) {
		super(message);
	}
}
