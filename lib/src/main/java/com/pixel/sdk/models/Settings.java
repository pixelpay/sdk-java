package com.pixel.sdk.models;

import com.pixel.sdk.base.Helpers;
import com.pixel.sdk.resources.Environment;

public class Settings {
	/**
	 * Merchant API auth key
	 */
	public String auth_key;

	/**
	 * Merchant API auth hash (MD5 of secret key)
	 */
	public String auth_hash;

	/**
	 * Merchant API platform auth user (SHA-512 of user email)
	 */
	public String auth_user;

	/**
	 * Merchant API endpoint URL
	 */
	public String endpoint;

	/**
	 * Merchant API environment
	 */
	public String environment;

	/**
	 * Settings response messages language
	 */
	public String lang;

	/**
	 * SDK identifier
	 */
	public String sdk;

	/**
	 * SDK identifier
	 */
	public String sdk_version;

	/**
	 * Initialize service
	 */
	public Settings() {
		this.endpoint = "https://pixelpay.app";
	}

	/**
	 * Setup API endpoint URL
	 *
	 * @param endpoint
	 */
	public void setupEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	/**
	 * Setup API credentials
	 *
	 * @param key
	 * @param hash
	 */
	public void setupCredentials(String key, String hash) {
		this.auth_key = key;
		this.auth_hash = hash;
	}

	/**
	 * Setup API platform user
	 *
	 * @param hash
	 */
	public void setupPlatformUser(String hash) {
		this.auth_user = hash;
	}

	/**
	 * Setup API environment
	 *
	 * @param env
	 */
	public void setupEnvironment(String env) {
		this.environment = env;
	}

	/**
	 * Setup defaults to Sandbox credentials
	 */
	public void setupSandbox() {
		this.endpoint = "https://pixel-pay.com";
		this.auth_key = "1234567890";
		this.auth_hash = Helpers.hash("MD5", "@s4ndb0x-abcd-1234-n1l4-p1x3l"); // MD5: @s4ndb0x-abcd-1234-n1l4-p1x3l

		this.environment = Environment.SANDBOX;
	}

	/**
	 * Setup response messages language
	 *
	 * @param lang
	 */
	public void setupLanguage(String lang) {
		this.lang = lang;
	}
}
