package com.pixel.sdk.models;

public class Item {
	/**
	 * Item identifier code or UPC/EAN
	 */
	public String code;

	/**
	 * Item product title
	 */
	public String title;

	/**
	 * Item per unit price
	 */
	public double price;

	/**
	 * Item quantity
	 */
	public int qty;

	/**
	 * Item tax amount per unit
	 */
	public double tax;

	/**
	 * Item total value
	 */
	public double total;

	/**
	 * Initialize model
	 */
	public Item() {
		this.price = 0.00;
		this.qty = 1;
		this.tax = 0.00;
		this.total = 0.00;
	}

	/**
	 * Totalize item price by quantity
	 * 
	 * @return
	 */
	public Item totalize() {
		this.total = this.price * this.qty;

		return this;
	}
}
