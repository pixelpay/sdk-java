package com.pixel.sdk.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Order {
	/**
	 * Order ID
	 */
	public String id;

	/**
	 * Order currency code alpha-3
	 */
	public String currency;

	/**
	 * Order total amount
	 */
	public double amount;

	/**
	 * Order total tax amount
	 */
	public double tax_amount;

	/**
	 * Order total shipping amount
	 */
	public double shipping_amount;

	/**
	 * Order summary of items or products
	 */
	public List<Item> content;

	/**
	 * Order extra properties
	 */
	public Map<String, String> extras;

	/**
	 * Order note or aditional instructions
	 */
	public String note;

	/**
	 * Order calback webhook URL
	 */
	public String callback_url;

	/**
	 * Order customer name
	 */
	public String customer_name;

	/**
	 * Order customer email
	 */
	public String customer_email;

	/**
	 * Initialize model
	 */
	public Order() {
		this.content = new ArrayList<>();
		this.extras = new HashMap<>();
	}

	/**
	 * Add item to content list of products/items
	 * 
	 * @param item
	 * @return
	 */
	public Order addItem(Item item) {
		this.content.add(item);
		this.totalize();

		return this;
	}

	/**
	 * Add extra property to order
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public Order addExtra(String key, Object value) {
		String parsedvalue = value.toString();
		this.extras.put(key, parsedvalue);

		return this;
	}

	/**
	 * Totalize order amounts and items
	 * 
	 * @return
	 */
	public Order totalize() {

		if (!this.content.isEmpty()) {
			this.amount = 0.00;
			this.tax_amount = 0.00;
			this.content.forEach(item -> {
				item.totalize();

				this.amount += item.total;
				this.tax_amount += (item.tax * item.qty);
			});
		}

		return this;
	}
}
