package com.pixel.sdk.models;

public class Billing {
	/**
	 * Customer billing address
	 */
	public String address;

	/**
	 * Customer billing country alpha-2 code (ISO 3166-1)
	 */
	public String country;

	/**
	 * Customer billing state alpha code (ISO 3166-2)
	 */
	public String state;

	/**
	 * Customer billing city
	 */
	public String city;

	/**
	 * Customer billing postal code
	 */
	public String zip;

	/**
	 * Customer billing phone
	 */
	public String phone;
}
