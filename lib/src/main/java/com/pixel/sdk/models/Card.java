package com.pixel.sdk.models;

import com.pixel.sdk.base.Helpers;

public class Card {
	/**
	 * Card number or PAN
	 */
	public String number;

	/**
	 * Card security code
	 */
	public String cvv2;

	/**
	 * Card expire month date (MM)
	 */
	public int expire_month;

	/**
	 * Card expire year date (YYYY)
	 */
	public int expire_year;

	/**
	 * Cardholder name
	 */
	public String cardholder;

	/**
	 * Get expire ISO format (YYMM)
	 * 
	 * @return
	 */
	public String getExpireFormat() {
		String year = (this.expire_year != 0) ? String.valueOf(this.expire_year) : "    ";
		String month = String.format("%02d", this.expire_month);

		return Helpers.trimValue(year.substring(year.length() - 2) + month);
	}
}
