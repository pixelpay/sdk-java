package com.pixel.sdk.resources;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Locations {
	/**
	 * Countries resource repository
	 */
	private static Map<String, String> countriesRepository;

	/**
	 * States resource repository
	 */
	private static Map<String, Object> statesRepository;

	/**
	 * Formats resource repository
	 */
	private static Map<String, Object> formatsRepository;

	/**
	 * Object mapper istance
	 */
	private static ObjectMapper mapper = new ObjectMapper();

	/**
	 * Prevent implicit public contructor
	 */
	private Locations() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * Verify respository is available
	 */
	private static void checkRepository() {
		if (Locations.countriesRepository == null) {
			try {
				InputStream countries = Locations.class.getClassLoader().getResourceAsStream("countries.json");
				InputStream states = Locations.class.getClassLoader().getResourceAsStream("states.json");
				InputStream formats = Locations.class.getClassLoader().getResourceAsStream("formats.json");

				countriesRepository = mapper.readValue(countries, new TypeReference<HashMap<String, String>>() {});
				statesRepository = mapper.readValue(states, new TypeReference<HashMap<String, Object>>() {});
				formatsRepository = mapper.readValue(formats, new TypeReference<HashMap<String, Object>>() {});
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Return a list of countries
	 * 
	 * @return
	 */
	public static Map<String, String> countriesList() {
		checkRepository();

		return countriesRepository;
	}

	/**
	 * Get states list by country ISO code
	 * 
	 * @param country_code
	 * @return
	 */
	public static Map<String, String> statesList(String country_code) {
		checkRepository();

		return mapper.convertValue(Locations.statesRepository.get(country_code), new TypeReference<HashMap<String, String>>() {});
	}

	/**
	 * Get phone and zip formats list by country ISO code
	 * 
	 * @param country_code
	 * @return
	 */
	public static Map<String, String> formatsList(String country_code) {
		checkRepository();

		return mapper.convertValue(Locations.formatsRepository.get(country_code), new TypeReference<HashMap<String, String>>() {});
	}
}
