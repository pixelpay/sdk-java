package com.pixel.sdk.resources;

public class Environment {
	public static final String LIVE = "live";
	public static final String TEST = "test";
	public static final String SANDBOX = "sandbox";
	public static final String STAGING = "staging";

	/**
	 * Prevent implicit public contructor
	 */
	private Environment() {
		throw new IllegalStateException("Utility class");
	}
}
