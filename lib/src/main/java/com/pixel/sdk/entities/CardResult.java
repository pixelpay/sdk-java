package com.pixel.sdk.entities;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pixel.sdk.base.Response;
import com.pixel.sdk.responses.SuccessResponse;

public class CardResult {
	/**
	 * Card status
	 */
	public String status;

	/**
	 * Card number masked
	 */
	public String mask;

	/**
	 * Card network brand
	 */
	public String network;

	/**
	 * Card type (debit/credit)
	 */
	public String type;

	/**
	 * Car bin number
	 */
	public String bin;
	
	/**
	 * Card last 4 numbers
	 */
	public String last;

	/**
	 * Card unique hash number
	 */
	public String hash;

	/**
	 * Billing address
	 */
	public String address;

	/**
	 * Billing country
	 */
	public String country;

	/**
	 * Billing state
	 */
	public String state;

	/**
	 * Billing city
	 */
	public String city;

	/**
	 * Billing postal code
	 */
	public String zip;

	/**
	 * Billing customer email
	 */
	public String email;

	/**
	 * Billing phone
	 */
	public String phone;

	/**
	 * Validate if response type is valid for parse
	 * 
	 * @param response
	 * @return
	 */
	public static boolean validateResponse(Response response) {
		return response instanceof SuccessResponse;
	}

	/**
	 * Convert success response to card entity
	 * 
	 * @param response
	 * @return
	 */
	public static CardResult fromResponse(SuccessResponse response) {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.convertValue(response.data, CardResult.class);
	}
}
