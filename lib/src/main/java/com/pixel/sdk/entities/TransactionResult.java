package com.pixel.sdk.entities;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pixel.sdk.base.Response;
import com.pixel.sdk.responses.ErrorResponse;
import com.pixel.sdk.responses.PaymentDeclinedResponse;
import com.pixel.sdk.responses.SuccessResponse;
import com.pixel.sdk.responses.TimeoutResponse;

public class TransactionResult {
	/**
	 * Transaction response type
	 */
	public String transaction_type;

	/**
	 * Transaction redeemed points
	 */
	public double transaction_redeemed_points;

	/**
	 * Approved amount on capture/sale
	 */
	public double transaction_approved_amount;

	/**
	 * Initial or registered transaction amount
	 */
	public double transaction_amount;

	/**
	 * Transaction AUTH reference code
	 */
	public String transaction_auth;

	/**
	 * Transacction network terminal ID
	 */
	public String transaction_terminal;

	/**
	 * Transaction network merchant ID
	 */
	public String transaction_merchant;

	/**
	 * CVV2 result response code
	 */
	public String response_cvn;

	/**
	 * Address verification code response
	 */
	public String response_avs;

	/**
	 * CAVV network evaluation result code
	 */
	public String response_cavv;

	/**
	 * Transaction identifier
	 */
	public String transaction_id;

	/**
	 * Transaction STAN, proccesor transacction identifier or transaction reference
	 */
	public String transaction_reference;

	/**
	 * Transaction result time
	 */
	public String transaction_time;

	/**
	 * Transaction result date
	 */
	public String transaction_date;

	/**
	 * Response is financial approved
	 */
	public boolean response_approved;

	/**
	 * Response fatal not completed or excecution interrupted
	 */
	public boolean response_incomplete;

	/**
	 * Proccesor response code
	 */
	public String response_code;

	/**
	 * Network response time
	 */
	public String response_time;

	/**
	 * Proccesor response message
	 */
	public String response_reason;

	/**
	 * Transaction installment type
	 */
	public String installment_type;

	/**
	 * Transaction installment value
	 */
	public String installment_months;

	/**
	 * Payment unique identifier
	 */
	public String payment_uuid;

	/**
	 * Payment integrity validation hash
	 */
	public String payment_hash;

	/**
	 * Validate if response type is valid for parse
	 * 
	 * @param response
	 * @return
	 */
	public static boolean validateResponse(Response response) {
		return response instanceof SuccessResponse
				|| response instanceof PaymentDeclinedResponse
				|| response instanceof ErrorResponse
				|| response instanceof TimeoutResponse;
	}

	/**
	 * Convert success response to transaction entity
	 * 
	 * @param response
	 * @return
	 */
	public static TransactionResult fromResponse(Response response) {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.convertValue(response.data, TransactionResult.class);
	}
}
