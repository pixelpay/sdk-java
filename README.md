# Java Standalone SDK

## ¿Como instalar?

Para obtener la versión de SDK con Java es necesario:

Ir a la ruta android/build.gradle y agregar en la sección de repositories lo siguiente:

``` gradle
buildscript{
  .
  .
  repositories{
    .
    .
    mavenCentral()
    maven { url 'https://jitpack.io' }
  }
}
```

El siguiente paso es dirigirte a build.gradle y en la sección de dependencies agregar:

``` gradle
dependencies{
    .
    .
    implementation 'org.bitbucket.pixelpay:sdk-java'
}
```